import itertools
from copy import deepcopy
from typing import List, Tuple, Callable, Union

from gavel.logic.logic import DefinedConstant, LogicElement, BinaryFormula, PredicateExpression, BinaryConnective, \
    FunctorExpression, Constant
from gavel.logic.problem import Problem, Sentence, AnnotatedFormula
from gavel.logic.proof import Proof, ProofStep

from utils import __stringify_proof, __stringify_sentence, __stringify_annotated_formula


def fulfillable(problem: Problem) -> bool:
    return __is_fulfillable(
        problem=problem,
        previous_proof=Proof(
            steps=[]
        ),
        current_proof=Proof(
            steps=[ProofStep(formula=DefinedConstant.VERUM)]
        ),
        iteration=1
    )


def __is_fulfillable(problem: Problem, previous_proof: Proof, current_proof: Proof, iteration: int) -> bool:
    print(f"\n\nIteration #{iteration}")
    print(f"Current Proof: {__stringify_proof(current_proof)}")

    if __has_terminated(previous_proof, current_proof):
        return not __conjecture_is_proven(problem.conjecture, current_proof)

    deduced = list(filter(
        lambda unknown: __can_be_deduced(unknown, current_proof),
        problem.premises
    ))

    for d in deduced:
        print(f"deduced: {__stringify_annotated_formula(d)}")

    return __is_fulfillable(
        problem=__reduce_problem(problem, deduced),
        previous_proof=current_proof,
        current_proof=__expand_knowledge(current_proof, deduced),
        iteration=iteration + 1
    )


def __can_be_deduced(unknown, proof: Proof) -> bool:
    if isinstance(unknown, AnnotatedFormula):
        print(f"attempting to deduce annotated formula: {__stringify_annotated_formula(unknown)}")
        return __can_be_deduced(unknown.formula.left, proof)

    elif isinstance(unknown, PredicateExpression):
        print(f"attempting to deduce predicate expression: {__stringify_sentence(unknown)}")
        pred_expr_steps = list(filter(lambda o: isinstance(o.formula, PredicateExpression), proof.steps))
        pred_exprs = list(map(lambda o: o.formula, pred_expr_steps))
        return any(str(unknown) == str(pred_expr) for pred_expr in pred_exprs)

    elif isinstance(unknown, DefinedConstant):
        print(f"attempting to deduce constant: {__stringify_sentence(unknown)}")
        constant_proof_steps = list(filter(lambda o: isinstance(o.formula, DefinedConstant), proof.steps))
        constants = list(map(lambda o: o.formula, constant_proof_steps))
        return unknown in constants

    elif isinstance(unknown, BinaryFormula):
        print(f"attempting to deduce binary formula: {__stringify_sentence(unknown)}")
        return __can_be_deduced(unknown.left, proof) and __can_be_deduced(unknown.right, proof)

    else:
        raise Exception(f"Unhandled type in __can_be_deduced: {__stringify_sentence(unknown)} @ {type(unknown)}")


def __expand_knowledge(proof: Proof, proven: List[LogicElement]) -> Proof:
    def __extract_knowledge(sentence_: LogicElement, accum_knowledge: List[LogicElement], accum_equis: List[Tuple]) -> \
    Tuple[List[LogicElement], List[Tuple]]:
        if isinstance(sentence_, AnnotatedFormula):
            return __extract_knowledge(sentence_.formula.right, accum_knowledge, accum_equis)

        elif isinstance(sentence_, BinaryFormula):
            if sentence_.operator == BinaryConnective.EQ:
                accum_equis.append((sentence_.left, sentence_.right))
                accum_equis.append((sentence_.right, sentence_.left))

                print(f"equivalence found: {__stringify_sentence(sentence_)}")
                return accum_knowledge, accum_equis

            else:
                raise Exception(f"Unhandled BinaryConnective in __extract_knowledge: {sentence_.operator}")

        elif isinstance(sentence_, (PredicateExpression, DefinedConstant)):
            return accum_knowledge + [sentence_], accum_equis

        else:
            raise Exception(f"Unhandled type in __extract_knowledge: {type(sentence_)}")

    def __apply_equivalence(equivalence: Tuple, formula: LogicElement) -> Union[LogicElement, None]:
        if formula is None:
            return None

        from_, to_ = equivalence

        if isinstance(formula, DefinedConstant):
            return None

        elif isinstance(formula, Constant):
            if str(formula) == str(from_):
                print(f"{__stringify_sentence(formula)} <==> {__stringify_sentence(to_)}")
                return to_
            return None

        elif isinstance(formula, (FunctorExpression, PredicateExpression)):
            new_arguments = [__apply_equivalence(equivalence, argument) or argument for argument in formula.arguments]
            if any(arg is None for arg in new_arguments):
                return None

            if "".join(map(str, new_arguments)) != "".join(map(str, formula.arguments)):
                predicate = PredicateExpression(
                    predicate=formula.predicate if isinstance(formula, PredicateExpression) else formula.functor,
                    arguments=new_arguments
                )

                print(f"{__stringify_sentence(formula)} <==> {__stringify_sentence(predicate)}")
                return predicate

            return None

        else:
            raise Exception(f"Unhandled type in __apply_equivalence {str(formula)} @ {type(formula)}")

    new_proof = Proof(
        premises=proof.premises[:],
        steps=proof.steps[:]
    )

    for sentence in proven:
        knowledge, equivalences = __extract_knowledge(sentence, list(), list())
        new_proof.steps.extend(map(ProofStep, knowledge))
        new_proof.premises.extend(equivalences)

    old_step_count, current_step_count = -1, len(new_proof.steps)

    while current_step_count != old_step_count:
        inferred_from_equivalences = filter(
            lambda o: o is not None,
            (__apply_equivalence(equivalence, step.formula) for equivalence in new_proof.premises for step in
             new_proof.steps)
        )
        new_equivalences = filter(
            lambda i: str(i) not in map(lambda step: str(step.formula), new_proof.steps),
            inferred_from_equivalences
        )

        old_step_count = len(new_proof.steps)
        new_proof.steps.extend(map(ProofStep, new_equivalences))
        current_step_count = len(new_proof.steps)
        print(f"deduced {current_step_count - old_step_count} new steps from equivalence: {list(new_equivalences)}")

    return new_proof


def __reduce_problem(problem: Problem, proven: List[LogicElement]) -> Problem:
    unknown = list(filter(
        lambda sentence: sentence not in proven,
        problem.premises
    ))

    return Problem(
        premises=unknown,
        conjecture=problem.conjecture
    )


def __conjecture_is_proven(conjecture: AnnotatedFormula, proof: Proof) -> bool:
    return any(conjecture.formula == knowledge.formula for knowledge in proof.steps)


def __has_terminated(s1: Proof, s2: Proof) -> bool:
    return len(s1.steps) == len(s2.steps)
    # all(step1.formula == step2.formula for step1, step2 in zip(s1.steps, s2.steps))
