fof(a1, axiom, (p(a) & p(b) & p(d)) => $false).
fof(a2, axiom, p(e)  => $false).
fof(a3, axiom, p(c) => p(a)).
fof(a4, axiom, p(c)   => $false).
fof(a5, axiom, p(a) => p(d)).
fof(c, conjecture, $false).
