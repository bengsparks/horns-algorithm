from sys import argv

from gavel.dialects.base.parser import ProblemParser
from gavel.dialects.tptp.parser import TPTPProblemParser

from horn import fulfillable
from utils import print_problem


def main(parser: ProblemParser, filepaths: list):
    for filepath in filepaths:
        for problem in parser.parse(open(filepath).read()):
            print_problem(filepath, problem)

            if fulfillable(problem):
                print("FULFILLABLE")
            else:
                print("NOT FULFILLABLE")


if __name__ == "__main__":
    main(TPTPProblemParser(), argv[1:])
