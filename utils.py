from gavel.logic.logic import LogicElement, BinaryFormula, DefinedConstant, PredicateExpression
from gavel.logic.problem import Problem, AnnotatedFormula
from gavel.logic.proof import Proof, ProofStep


def print_problem(filepath: str, problem: Problem):
    print(f"===== {filepath} =====\n")
    print(f"=== premises ===")
    for premise in problem.premises:
        print(f"{premise.formula}")

    print(f"\n=== conjecture ===")
    print(f"{problem.conjecture.formula}\n\n")


def __stringify_annotated_formula(formula: AnnotatedFormula) -> str:
    return __stringify_sentence(formula.formula)


def __stringify_proof(proof: Proof) -> str:
    return ", ".join(__stringify_sentence(step) for step in proof.steps)


def __stringify_sentence(sentence: LogicElement) -> str:
    if isinstance(sentence, ProofStep):
        return __stringify_sentence(sentence.formula)

    return str(sentence)
    # if isinstance(sentence, BinaryFormula):
    #     return str(sentence)
    #
    # elif isinstance(sentence, ProofStep):
    #     return __stringify_sentence(sentence.formula)
    #
    # elif isinstance(sentence, DefinedConstant):
    #     return str(sentence)
    #
    # elif isinstance(sentence, PredicateExpression):
    #     return str(sentence)
    #
    # else:
    #     raise Exception(f"Unhandled type in {__stringify_sentence.__name__}: {type(sentence)}")
